use clap::Parser;
use std::boxed::Box;
use std::fs;
use std::path::{Path, PathBuf};
use std::result::Result;
use symlink::symlink_file;
use walkdir::WalkDir;

fn merge(source: &Path, dest: &Path) -> Result<(), &'static str> {
    // Step1 - get all mod(package) list - absolute directories paths
    // Step2 - Iterate tha list and walkdir through all files
    // Step3 - Create symlink to each file to dest. If any directory not exist - just create directory and put symlink here

    let source_path = Path::new(&source);
    let dest_path = Path::new(&dest);

    if !source_path.exists() {
        return Err("Source path not found!");
    }

    if !dest_path.exists() {
        return Err("Destination path not exist!");
    }

    for file in fs::read_dir(&source).unwrap() {
        let current_path = file.unwrap().path();

        if current_path.is_dir() {
            let pkg_name = &current_path.strip_prefix(&source).unwrap();

            // DBG
            println!("New package found: {}", &pkg_name.display());

            for entry in WalkDir::new(&current_path) {
                let current_file = entry.unwrap();

                let file_abs_path = current_file.path();
                let file_rel_path = file_abs_path.strip_prefix(&current_path).unwrap();

                if file_abs_path.is_dir() && file_abs_path != current_path {
                    let dest_path = Path::new(dest).join(file_rel_path);

                    // Here we check and create directory
                    if dest_path.exists() {
                        continue;
                    }

                    fs::create_dir_all(&dest_path).unwrap();
                }
                if file_abs_path.is_file() {
                    let dest_path = Path::new(dest).join(file_rel_path);

                    // Here we check and create symlink
                    if dest_path.exists() {
                        if dest_path.is_symlink() || dest_path.is_file() {
                            // DBG
                            println!(
                                "Package file {} conflict found! Symlink will be replaced!",
                                &file_rel_path.display()
                            );

                            fs::remove_file(&dest_path).unwrap();
                        }
                    }
                    symlink_file(file_abs_path, &dest_path).unwrap();
                }
            }
        }
    }
    return Ok(());
}

fn parse_mod_organizer_profile(profile_path: PathBuf) -> Result<Vec<String>, &'static str> {
    if !&profile_path.exists() {
        return Err("Profile path not found!");
    }

    let content = fs::read_to_string(profile_path).unwrap();

    let lines: Vec<&str> = content.lines().collect();

    let result: Vec<String> = lines
        .iter()
        .filter(|line| line.starts_with("+") && !line.ends_with("_separator"))
        .rev()
        .map(|line| line.to_string())
	.collect();

    Ok(result)
}

#[derive(Parser, Debug)]
struct Args {
    #[arg(short, long)]
    src: Option<PathBuf>,

    #[arg(short, long)]
    dest: Option<PathBuf>,

    #[arg(short, long)]
    profile: Option<PathBuf>,
}

fn main() {
    let args = Args::parse();

    if let Some(src) = args.src.as_deref() {
        if !&src.exists() {
            println!("Source path {} does not exist!", &src.display());
            return;
        }
        println!("Source path: {}", src.display());

        if let Some(dest) = args.dest.as_deref() {
            if !&dest.exists() {
                println!("Destination path {} does not exist!", &dest.display());
                return;
            }
            println!("Destination path: {}", dest.display());

            merge(src, dest).unwrap();

            return;
        }

        println!("Destination path not provided!");
    }
}
